# info from https://www.purina.com/cats/cat-breeds
tmp = {"Size": "",
       "Coat": "",
       "Color": "",
       "Info": ""}

exotic_shorthair = {"Size": "Medium to large, with males weighing 7 to 14 pounds and females weighing 6 to 10 pounds",
                    "Coat": "Short, plush, thick",
                    "Color": "White, black, blue, red, cream, chocolate, lilac, "
                             "silver, plus various patterns and shadings",
                    "Info": "Known as the lazy man’s Persian, the Exotic Shorthair has the body type and easygoing "
                            "nature of the Persian but without the coat length and need for daily grooming. With her "
                            "thick, dense, plush short hair and round face, the Exotic Shorthair has a soft teddy "
                            "bear look. This sweet feline is affectionate and loyal. The breed is athletic, "
                            "fun-loving, yet also quiet and sensitive."}

manx = {"Size": "Medium to large, with males weighing from 9 to 13 pounds and females weighing from 7 to 11 pounds",
        "Coat": "Longhair — medium, dense, soft, silky; shorthair — short, dense, glossy",
        "Color": "White, black, blue, red, cream and silver, plus various patterns and shadings",
        "Info": "When she’s not hunting bugs or rodents or standing guard, the Manx is an affectionate, even-tempered "
                "and playful cat. She loves to follow her favorite person from room to room and curling up on their "
                "lap for a snooze. Your Manx will even carry on a conversation with you in her quiet trill. When "
                "exposed to new activities, people and animals from a young age, the Manx is adaptable and even "
                "enjoys meeting and greeting new people. She’s smart enough to learn tricks like fetch and walking on "
                "a leash. Your Manx makes a great road-trip companion, as she enjoys riding in cars, and she likes to "
                "play with water. She can learn to open doors and turn on faucets. As a people-oriented cat, "
                "she needs lots of attention so don’t leave her alone for hours at a time."}

siberian = {"Size": "Medium to large weighing 8 to 17 pounds",
            "Coat": "Semi-long",
            "Color": "White, black, blue, red, cream and silver, plus various patterns and shadings",
            "Info": "This friendly and affectionate feline will follow you around as you go about your day, and purr "
                    "in your lap as you comb her coat. Siberian Cats love their humans but aren’t shy around "
                    "strangers. They’re an active and playful breed, enjoying games like fetch and learning tricks "
                    "that stimulate them mentally. Their athleticism allows them to climb and perch themselves from "
                    "the highest heights. This bold cat loves to play with water and gets along well with other pets "
                    "and children."}
