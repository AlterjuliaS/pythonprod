import telebot
from telebot import types
from topic_12_telegram_bot.examples.cats.cat_breeds_info import *

token = '1184152928:AAFvsnRFoML33KdHGu4xSx_GgQXAZrDueK0'
bot = telebot.TeleBot(token)

files = {}


@bot.message_handler(commands=['get_everything'])
def get_everything(message):
    for message_id, document_id in files.get(message.from_user.id, []):
        bot.forward_message(message.chat.id, message.chat.id, message_id)


@bot.message_handler(content_types=['photo', 'voice', 'document'])
def save_file(message):
    global files
    if files.get(message.from_user.id) is None:
        files[message.from_user.id] = []
    files[message.from_user.id].append((message.id, message.document.file_id))


@bot.message_handler(commands=['start'])
def new_member(message):
    bot.send_message(message.from_user.id, f'Привет!!! {message.from_user.first_name}')


if __name__ == '__main__':
    print('Starting bot...')
    bot.polling(none_stop=True, interval=0)
