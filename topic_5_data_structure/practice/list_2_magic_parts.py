def magic_parts(my_list):
    """
    Функция magic_parts.

    Принимает 1 аргумент: список my_list.

    Возвращает список, который состоит из
        [первые 2 элемента my_list]
        + [последний элемент my_list]
        + [количество элементов в списке my_list].

    Пример: входной список [1, 2, ‘aa’, ‘mm’], результат [1, 2, ‘mm’, 4].

    Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
    Если список пуст, то возвращать строку 'Empty list!'.
    """

    if type(my_list) != list:
        return 'First arg must be list!'
    elif len(my_list) == 0:
        return 'Empty list!'

    # return my_list[:2] + [my_list[-1]] + [len(my_list)]

    res_list = []
    res_list.extend(my_list[:2])
    res_list.append(my_list[-1])
    res_list.append(len(my_list))
    return res_list


if __name__ == '__main__':
    print(magic_parts([1, 2, 3]))
