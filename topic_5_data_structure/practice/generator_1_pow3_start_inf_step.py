import itertools


def pow3_start_inf_step(start, step):
    """
    Функция pow3_start_inf_step.

    Принимает 2 аргумента: число start, step.

    Возвращает генератор-выражение состоящий из
    значений в 3 степени от start до бесконечности с шагом step.

    Пример: start=3, step=2 результат 3^3, 5^3, 7^3, 9^3 ... (infinity).

    Если start или step не являются int, то вернуть строку 'Start and Step must be int!'.
    """

    if type(start) != int or type(step) != int:
        return 'Start and Step must be int!'

    return (x ** 3 for x in itertools.count(start=start, step=step))


if __name__ == '__main__':
    my_gen = pow3_start_inf_step(3, 2)

    for i in range(2000):
        print(next(my_gen))
