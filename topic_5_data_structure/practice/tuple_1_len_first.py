def len_first(my_tuple):
    """
    Функция len_first.

    Принимает 1 аргумент: кортеж my_tuple.

    Возвращает кортеж состоящий из длины кортежа и первого элемента кортежа.

    Пример: (‘55’, ‘aa’, 66), результат (3, ‘55’).

    Если вместо tuple передано что-то другое, то возвращать строку 'Must be tuple!'.
    Если tuple пуст, то возвращать строку 'Empty tuple!'.
    """

    if type(my_tuple) != tuple:
        return 'Must be tuple!'
    elif len(my_tuple) == 0:
        return 'Empty tuple!'

    return len(my_tuple), my_tuple[0]


if __name__ == '__main__':
    my_result = len_first((1, 3, 4, 5))

    my_len, first_elem = len_first((1, 3, 4, 5))

    print(f'my_result={my_result}')
    print(f'my_len={my_len}')
    print(f'first_elem={first_elem}')

    print(my_result[1])
    # my_result[1] = 6    # TypeError: 'tuple' object does not support item assignment
    my_result = (5, 6, 7)
    my_result = list(my_result)
    my_result[1] = 9
    pass
