def get_numbers_by_name(name_num: dict, name: str):
    """
    Функция get_numbers_by_name.

    Принимает 2 аргумента:
        словарь содержащий {имя: телефон},
        слово (имя) для поиска в словаре.

    Возвращает все телефоны по имени, если такое имя есть в словаре, иначе "Такого имени нет в каталоге!".

    Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
    Если словарь пустой, то возвращать строку 'Dictionary is empty!'.

    Если вместо строки для поиска передано что-то другое, то возвращать строку 'Name must be str!'.
    Если строка для поиска пустая, то возвращать строку 'Name is empty!'.
    """

    if type(name_num) != dict:
        return 'Dictionary must be dict!'
    elif len(name_num) == 0:
        return 'Dictionary is empty!'

    if type(name) != str:
        return 'Name must be str!'
    elif len(name) == 0:
        return 'Name is empty!'

    return name_num.get(name, "Такого имени нет в каталоге!")


if __name__ == '__main__':
    my_result = get_numbers_by_name({'pete': '777-44', 'ira': '1234-555'}, 'ira')
    print(my_result)
