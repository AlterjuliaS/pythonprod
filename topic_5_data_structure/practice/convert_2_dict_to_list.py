def dict_to_list(my_dict: dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает список: [
        список ключей,
        список значений,
        количество элементов в списке ключей в степени 3,
        количество элементов в списке значений,
        хотя бы один ключ равен одному из значений (True | False).
    ]

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

    Если dict пуст, то возвращать пустой list.
    """
    dict_keys = list(my_dict.keys())
    dict_vals = list(my_dict.values())
    res_list = [dict_keys, dict_vals, len(dict_keys), len(dict_vals)]

    for key in dict_keys:
        if key in dict_vals:
            res_list.append(True)
            break
    else:
        res_list.append(False)

    return res_list


if __name__ == '__main__':
    print(dict_to_list({1: 1, 2: 2, 11: 1, 12: 2}))
