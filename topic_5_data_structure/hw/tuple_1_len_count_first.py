def len_count_first(my_tuple, word):
    """
    Функция len_count_first.

    Принимает 2 аргумента: кортеж my_tuple и строку word.

    Возвращает кортеж состоящий из
        длины кортежа,
        количества word в кортеже my_tuple,
        первого элемента кортежа.

    Пример:
        my_tuple=('55', 'aa', '66')
        word = '66'
        результат (3, 1, '55').

    Если вместо tuple передано что-то другое, то возвращать строку 'First arg must be tuple!'.
    Если вместо строки передано что-то другое, то возвращать строку 'Second arg must be str!'.
    Если tuple пуст, то возвращать строку 'Empty tuple!'.
    """
    if type(my_tuple) != tuple:
        return 'First arg must be tuple!'
    elif type(word) != str:
        return 'Second arg must be str!'
    elif len(my_tuple) == 0:
        return 'Empty tuple!'
    else:
        len_tuple = len(my_tuple)
        count_word = my_tuple.count(word)
        first_elem = my_tuple[0]
        return len_tuple, count_word, first_elem


if __name__ == '__main__':
    print(len_count_first((1, 2, 3, 4), '1'))
