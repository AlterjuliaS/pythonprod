def magic_mul(my_list):
    """
    Функция magic_mul.

    Принимает 1 аргумент: список my_list.

    Возвращает список, который состоит из
        [первого элемента my_list]
        + [три раза повторенных списков my_list]
        + [последнего элемента my_list].

    Пример:
        входной список [1,  ‘aa’, 99]
        результат [1, 1,  ‘aa’, 99, 1,  ‘aa’, 99, 1,  ‘aa’, 99, 99].

    Если вместо списка передано что-то другое, то возвращать строку 'Must be list!'.
    Если список пуст, то возвращать строку 'Empty list!'.
    """
    if type(my_list) != list:
        return 'Must be list!'
    elif len(my_list) == 0:
        return 'Empty list!'
    else:
        my_result = []
        my_result.append(my_list[0])
        for i in range(3):
            my_result.extend(my_list)
        my_result.append(my_list[-1])
    return my_result


if __name__ == '__main__':
    print(magic_mul([1, 2, 3, 4]))
