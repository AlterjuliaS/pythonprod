def zip_colour_shape(colors: list, forms: tuple):
    """
    Функция zip_colour_shape.
    
    Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).
    
    Возвращает список с парами значений из каждого аргумента.
    
    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.
    
    Если list пуст, то возвращать строку 'Empty list!'.
    Если tuple пуст, то возвращать строку 'Empty tuple!'.
    
    Если list и tuple различного размера, обрезаем до минимального (стандартный zip).
    """
    if type(colors) != list:
        return 'First arg must be list!'
    elif len(colors) == 0:
        return 'Empty list!'

    if type(forms) != tuple:
        return 'Second arg must be tuple!'
    elif len(forms) == 0:
        return 'Empty tuple!'

    return list(zip(colors, forms))


if __name__ == '__main__':
    print(zip_colour_shape(['зеленый', 'красный'], ('квадрат', 'круг',)))
    print(zip_colour_shape(['зеленый', 'красный'], None))
