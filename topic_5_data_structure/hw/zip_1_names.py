def zip_names(name, family):
    """
    Функция zip_names.

    Принимает 2 аргумента: список с именами и множество с фамилиями.
    
    Возвращает список с парами значений из каждого аргумента.
    
    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.
    
    Если list пуст, то возвращать строку 'Empty list!'.
    Если set пуст, то возвращать строку 'Empty set!'.
    
    Если list и set различного размера, обрезаем до минимального (стандартный zip).
    """
    if type(name) != list:
        return 'First arg must be list!'
    elif len(name) == 0:
        return 'Empty list!'

    if type(family) != set:
        return 'Second arg must be set!'
    elif len(family) == 0:
        return 'Empty set!'

    return list(zip(name, family))


if __name__ == '__main__':
    print(zip_names(['Вася', 'Коля'], {'Пупкин', 'Курочкин'}))
    print(zip_names([], {'Пупкин', 'Курочкин'}))
    print(zip_names(['Вася', 'Коля'], set()))
    print(zip_names(['Вася', 'Коля'], None))
    print(zip_names(None, {'Пупкин', 'Курочкин'}))
