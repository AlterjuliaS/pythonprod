from FinalProject.dishes import Dishes


class Fish(Dishes):
    """
    Рыбные блюда
    """
    dish_type = 'Рыбные блюда'

    def __init__(self, cooking_method, kind_of_fish, price, calorie, proteins, fats, carbohydrates):
        super().__init__(price, calorie, proteins, fats, carbohydrates)
        self.cooking_method = cooking_method
        self.kind_of_fish = kind_of_fish

