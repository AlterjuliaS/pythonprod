class Director:
    def __init__(self, f_name, l_name, patronymic, age):
        self.f_name = f_name
        self.l_name = l_name
        self.patronymic = patronymic
        self.age = age

    def __str__(self):
        res_str = ""
        res_str += f"Name: {self.f_name} | "
        res_str += f"Last name: {self.l_name} |"
        res_str += f"Patronymic: {self.patronymic} |"
        res_str += f"Age: {self.age}"
        return res_str




