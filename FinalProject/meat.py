from FinalProject.dishes import Dishes


class Meat(Dishes):
    """
    Мясные блюда
    """
    dish_type = 'Мясные блюда'

    def __init__(self, kind_of_meat, price, calorie, proteins, fats, carbohydrates):
        super().__init__(price, calorie, proteins, fats, carbohydrates)
        self.kind_of_meat = kind_of_meat
