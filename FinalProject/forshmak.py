from FinalProject.fish import Fish


class Forshmak(Fish):
    """
    Форшмак
    """

    def __init__(self, cooking_method, kind_of_fish, country, price, calorie, proteins, fats, carbohydrates):
        super().__init__(cooking_method, kind_of_fish, price, calorie, proteins, fats, carbohydrates)
        self.country = country

    def __str__(self):
        return f"Forshmak (Country: {self.country} | {super().__str__()}"
