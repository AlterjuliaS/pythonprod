from FinalProject.fish import Fish


class FishCutlet(Fish):
    """
    Рыбные котлеты
    """

    def __init__(self, cooking_method, kind_of_fish, kind_of_cutlet, price, calorie, proteins, fats, carbohydrates):
        super().__init__(cooking_method, kind_of_fish, price, calorie, proteins, fats, carbohydrates)
        self.kind_of_cutlet = kind_of_cutlet

    def __str__(self):
        return f"Fish Cutlet (Kind of cutlet: {self.kind_of_cutlet} | {super().__str__()}"
