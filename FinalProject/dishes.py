class Dishes:
    """
    Блюда
    """
    def __init__(self, price, calorie, proteins, fats, carbohydrates):
        self.price = price
        self.calorie = calorie
        self.proteins = proteins
        self.fats = fats
        self.carbohydrates = carbohydrates

    def __str__(self):
        return f"price: {self.price} | calorie: {self.calorie} | proteins: {self.proteins} |" \
               f" fats: {self.fats} | carbohydrates: {self.carbohydrates}"
