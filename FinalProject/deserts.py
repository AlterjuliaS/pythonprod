from FinalProject.dishes import Dishes


class Deserts(Dishes):
    """
    Десерты
    """
    dish_type = 'Десерты'

    def __init__(self, taste, sweetness, price, calorie, proteins, fats, carbohydrates):
        super().__init__(price, calorie, proteins, fats, carbohydrates)
        self.taste = taste
        self.sweetness = sweetness

