import telebot
from telebot import types
import pickle

from FinalProject.director import Director
from FinalProject.cafe import Cafe
from FinalProject.tea import Tea
from FinalProject.coffee import Coffee
from FinalProject.beer import Beer
from FinalProject.ice_cream import IceCream
from FinalProject.steak import Steak
from FinalProject.chop import Chop
from FinalProject.cutlet import Cutlet
from FinalProject.forshmak import Forshmak
from FinalProject.fried_fish import FriedFish
from FinalProject.fish_cutlet import FishCutlet

token = '1474601569:AAHnFC6t7Zu-LPm-MSBsTwr2ejQ-2RPAVrk'
bot = telebot.TeleBot(token)

cafe_state = {}

user_state = {}

user_current_cafe = {}

hideBoard = types.ReplyKeyboardRemove()

state_filename = 'bot_state.pkl'


def save_state():
    with open(state_filename, 'wb') as file:
        pickle.dump({'cafe_state': cafe_state,
                     'user_current_cafe': user_current_cafe,
                     'user_state': user_state},
                    file)


def load_state():
    global cafe_state
    global user_state
    global user_current_cafe

    try:
        with open(state_filename, 'rb') as file:
            bot_state = pickle.load(file)
            cafe_state = bot_state.get('cafe_state', {})
            user_state = bot_state.get('user_state', {})
            user_current_cafe = bot_state.get('user_current_cafe', {})
    except FileNotFoundError as ex:
        print(f'Error loading backup file: {ex}')
        cafe_state = {}
        user_state = {}
        user_current_cafe = {}


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    global user_current_cafe

    bot.answer_callback_query(callback_query_id=call.id, text="Информация о кафе")

    current_cafe = cafe_state[call.from_user.id][int(call.data) - 1]
    user_current_cafe[call.from_user.id] = current_cafe
    save_state()

    cafe_info = current_cafe.get_dishes_cafe_str()

    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(call.message.chat.id,
                     (cafe_info if len(cafe_info) > 0
                      else "Нет ни одного блюда в меню!") + "\n\n Хотите добавить?",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(call.message, add_dishes_yes_no_answer)


def add_dishes_yes_no_answer(message):
    if message.text == "Да":
        current_cafe = user_current_cafe.get(message.from_user.id)
        if current_cafe is None:
            bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                                   "Для просмотра списка кафе, команда /cafe_list",
                             reply_markup=hideBoard)
        else:
            dishes_list_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                             one_time_keyboard=True,
                                                             row_width=2)
            dishes_list_keyboard.row("Чай", "Кофе", "Пиво", "Мороженое",
                                     "Стейк", "Мясная котлета", "Отбивная",
                                     "Жареная рыба", "Рыбная котлета", "Форшмак")
            bot.send_message(message.chat.id, "Какое блюдо добавить?",
                             reply_markup=dishes_list_keyboard)
            bot.register_next_step_handler(message, add_choose_dish_type)

    else:
        bot.send_message(message.from_user.id, "Ок, как хотите!", reply_markup=hideBoard)


def add_choose_dish_type(message):
    if message.text == "Чай":
        bot.send_message(message.chat.id, "Введите характеристики блюда: kind_of_tea(черный / зеленый),"
                                          " alcohol, caffeine,"
                                          " temperature, price, calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_tea)

    elif message.text == "Кофе":
        bot.send_message(message.chat.id,
                         "Введите характеристики блюда: kind_of_coffee(каппучино, эспрессо ...),"
                         " with_milk, alcohol, caffeine,"
                         " temperature, price, calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_coffee)

    elif message.text == "Пиво":
        bot.send_message(message.chat.id, "Введите характеристики блюда: kind_of_beer (темное / светлое)"
                                          ", country, alcohol, caffeine,"
                                          " temperature, price, calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_beer)

    elif message.text == "Мороженое":
        bot.send_message(message.chat.id, "Введите характеристики блюда: taste, sweetness, "
                                          "fat_content, price, calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_icecream)

    elif message.text == "Стейк":
        bot.send_message(message.chat.id, "Введите характеристики блюда: kind_of_meat, rare, price, "
                                          "calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_steak)

    elif message.text == "Мясная котлета":
        bot.send_message(message.chat.id, "Введите характеристики блюда: kind_of_meat, sauce, price, "
                                          "calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_cutlet)

    elif message.text == "Отбивная":
        bot.send_message(message.chat.id, "Введите характеристики блюда: kind_of_meat, cooking_method, "
                                          "price, calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_chop)

    elif message.text == "Жареная рыба":
        bot.send_message(message.chat.id, "Введите характеристики блюда: cooking_method, kind_of_fish, description, "
                                          "price, calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_fried_fish)

    elif message.text == "Рыбная котлета":
        bot.send_message(message.chat.id, "Введите характеристики блюда: cooking_method, kind_of_fish, kind_of_cutlet,"
                                          " price, calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_fish_cutlet)

    elif message.text == "Форшмак":
        bot.send_message(message.chat.id, "Введите характеристики блюда: cooking_method, kind_of_fish, country, "
                                          "price, calorie, proteins, fats, carbohydrates",
                         reply_markup=hideBoard)

        bot.register_next_step_handler(message, add_forshmak)


def add_tea(message):
    kind_of_tea, alcohol, caffeine, temperature, price, calorie, proteins, fats, carbohydrates \
        = message.text.split(', ')
    new_tea = Tea(kind_of_tea, alcohol, caffeine, temperature, price, calorie, proteins, fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_tea)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_tea}")
        save_state()


def add_coffee(message):
    kind_of_coffee, with_milk, alcohol, caffeine, temperature, price, calorie, proteins, fats, carbohydrates \
        = message.text.split(', ')
    new_coffee = Coffee(kind_of_coffee, with_milk, alcohol, caffeine, temperature, price, calorie, proteins,
                        fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_coffee)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_coffee}")
        save_state()


def add_beer(message):
    kind_of_beer, country, alcohol, caffeine, temperature, price, calorie, proteins, \
    fats, carbohydrates \
        = message.text.split(', ')
    new_beer = Beer(kind_of_beer, country, alcohol, caffeine, temperature, price, calorie, proteins,
                    fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_beer)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_beer}")
        save_state()


def add_icecream(message):
    taste, sweetness, fat_content, price, calorie, proteins, fats, carbohydrates \
        = message.text.split(', ')
    new_icecream = IceCream(taste, sweetness, fat_content, price, calorie, proteins, fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_icecream)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_icecream}")
        save_state()


def add_steak(message):
    kind_of_meat, rare, price, calorie, proteins, fats, carbohydrates = message.text.split(', ')
    new_steak = Steak(kind_of_meat, rare, price, calorie, proteins, fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_steak)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_steak}")
        save_state()


def add_cutlet(message):
    kind_of_meat, sauce, price, calorie, proteins, fats, carbohydrates = message.text.split(', ')
    new_cutlet = Cutlet(kind_of_meat, sauce, price, calorie, proteins, fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_cutlet)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_cutlet}")
        save_state()


def add_chop(message):
    kind_of_meat, cooking_method, price, calorie, proteins, fats, carbohydrates = message.text.split(', ')
    new_chop = Chop(kind_of_meat, cooking_method, price, calorie, proteins, fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_chop)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_chop}")
        save_state()


def add_fried_fish(message):
    cooking_method, kind_of_fish, description, price, calorie, proteins, fats, carbohydrates \
        = message.text.split(', ')
    new_fried_fish = FriedFish(cooking_method, kind_of_fish, description, price, calorie, proteins,
                               fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_fried_fish)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_fried_fish}")
        save_state()


def add_fish_cutlet(message):
    cooking_method, kind_of_fish, kind_of_cutlet, price, calorie, proteins, fats, carbohydrates \
        = message.text.split(', ')
    new_fish_cutlet = FishCutlet(cooking_method, kind_of_fish, kind_of_cutlet, price,
                                 calorie, proteins, fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_fish_cutlet)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_fish_cutlet}")
        save_state()


def add_forshmak(message):
    cooking_method, kind_of_fish, country, price, calorie, proteins, fats, carbohydrates \
        = message.text.split(', ')
    new_forshmak = Forshmak(cooking_method, kind_of_fish, country, price, calorie, proteins, fats, carbohydrates)
    current_cafe = user_current_cafe.get(message.from_user.id)
    if current_cafe is None:
        bot.send_message(message.from_user.id, "Кафе не выбрано! "
                                               "Для просмотра списка кафе, команда /cafe_list")
    else:
        current_cafe.add_dish(new_forshmak)
        bot.send_message(message.from_user.id, f"Блюдо добавлено:\n{new_forshmak}")
        save_state()


@bot.message_handler(commands=['add_cafe'])
def create_cafe_cmd(message):
    if user_state.get(message.from_user.id) is None:
        bot.send_message(message.chat.id,
                         "Давайте познакомимся!\n"
                         "Введите значения (через запятую с пробелом): f_name, l_name, "
                         "patronymic, age")
        bot.register_next_step_handler(message, add_user_to_user_state)
    else:
        bot.send_message(message.chat.id, f'Мы уже знакомы.\n{user_state.get(message.from_user.id)}\n '
                                          f'перейдем к следующему шагу\n\n'
                                          f'Напиши название нового кафе:')
        bot.register_next_step_handler(message, add_new_user_cafe)


def add_user_to_user_state(message):
    f_name, l_name, patronymic, age = message.text.split(', ')
    new_user = Director(f_name=f_name,
                        l_name=l_name,
                        patronymic=patronymic,
                        age=age)
    user_state[message.from_user.id] = new_user
    bot.send_message(message.chat.id, f'Приятно познакомиться.\n{new_user}\n '
                                      'Перейдем к следующему шагу!\n\n Напиши название нового кафе:')
    bot.register_next_step_handler(message, add_new_user_cafe)

    save_state()


def add_new_user_cafe(message):
    new_cafe = Cafe(message.text, user_state.get(message.from_user.id))

    if cafe_state.get(message.from_user.id) is None:
        cafe_state[message.from_user.id] = []
    cafe_state[message.from_user.id].append(new_cafe)
    bot.send_message(message.chat.id, f"Кафе добавлено!\n{new_cafe}")

    save_state()


@bot.message_handler(commands=['cafe_list'])
def cafe_list_cmd(message):
    user_cafes = cafe_state.get(message.from_user.id, [])
    if len(user_cafes) == 0:
        bot.send_message(message.chat.id, "У вас нет ни одного кафе!\n "
                                          "Для добавления используйте /add_cafe")
    else:
        markup = types.InlineKeyboardMarkup()

        for index, cafe in enumerate(user_cafes):
            markup.add(types.InlineKeyboardButton(text=str(cafe), callback_data=index + 1))

        bot.send_message(message.chat.id, "Выберите кафе для просмотра", reply_markup=markup)


@bot.message_handler(commands=['help'])
def help_cmd(message):
    # answer = f"<b>Привет {message.from_user.first_name}</b>!\n" \
    answer = f"\nДля получения списка кафе команда /cafe_list" \
             f"\nДля создания нового кафе команда /add_cafe"
    bot.send_message(message.chat.id, answer, parse_mode='html')


@bot.message_handler(commands=['start'])
def new_user(message):
    bot.send_message(message.from_user.id, f'Привет, {message.from_user.first_name}!\n'
                                           f'Для получения списка команд введите /help')


if __name__ == '__main__':
    print('Loading state...')
    load_state()
    print('Starting bot...')
    bot.polling(none_stop=True, interval=0)
