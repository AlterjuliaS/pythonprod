from FinalProject.dishes import Dishes


class Drinks(Dishes):
    """
    Напитки
    """
    dish_type = 'Напитки'

    def __init__(self, alcohol, caffeine, temperature, price, calorie, proteins, fats, carbohydrates):
        super().__init__(price, calorie, proteins, fats, carbohydrates)
        self.alcohol = alcohol
        self.caffeine = caffeine
        self.temperature = temperature


