import pytest
from FinalProject.beer import Beer
from FinalProject.chop import Chop
from FinalProject.coffee import Coffee
from FinalProject.cutlet import Cutlet
from FinalProject.fish_cutlet import FishCutlet
from FinalProject.forshmak import Forshmak
from FinalProject.fried_fish import FriedFish
from FinalProject.ice_cream import IceCream
from FinalProject.steak import Steak
from FinalProject.tea import Tea
from FinalProject.cafe import Cafe


def test_init():
    title = 'Cafe'
    director = 'Vasiliy Alibabaevich'

    cafe = Cafe(title=title, director=director)

    assert cafe.cafe == dict()
    assert cafe.title == title
    assert cafe.director == director


def test_add_dishes():
    cafe = Cafe('Cafe', 'Vasiliy Alibabaevich')
    ice_cream_strawberry = IceCream('strawberry', 'sweet', 50, 70, 700, 15, 40, 155)
    steak_beef = Steak('beef', 'medium', 100, 500, 150, 50, 70)
    cutlet_pork = Cutlet('pork', 'None', 100, 400, 70, 300, 700)
    chop_beef = Chop('beef', 'for a couple', 100, 400, 70, 300, 700)
    black_tea = Tea('black', 0, 0, 'hot', 50, 100, 5, 5, 150)
    espresso_coffee = Coffee('Arabika', 'No', 0, 'Yes', 'hot', 450, 300, 10, 50, 150)
    light_beer = Beer('Light', 'Czech Republic', 5, 0, 'cold', 250, 500, 0, 40, 600)
    dark_beer = Beer('Dark', 'Czech Republic', 6, 0, 'cold', 350, 500, 0, 40, 600)
    fried_fish_cod = FriedFish('Fried', 'Cod', 'Fillet', 1000, 900, 300, 200, 700)
    fried_fish_pike = FriedFish('Fried', 'Pike', 'Steak', 1000, 900, 300, 200, 700)
    forshmak_jewish = Forshmak('Mixed', 'Herring', 'Israel', 150, 800, 200, 400, 900)
    forshmak_germany = Forshmak('Baked', 'Herring and meat', 'Germany', 300, 1000, 500, 200, 300)
    fish_cutlet_steam = FishCutlet('Steam', 'Cod', 'Chopped', 700, 800, 300, 300, 200)

    cafe.add_dish(ice_cream_strawberry)
    cafe.add_dish(steak_beef)
    cafe.add_dish(cutlet_pork)
    cafe.add_dish(chop_beef)
    cafe.add_dish(black_tea)
    cafe.add_dish(espresso_coffee)
    cafe.add_dish(light_beer)
    cafe.add_dish(dark_beer)
    cafe.add_dish(fried_fish_cod)
    cafe.add_dish(fried_fish_pike)
    cafe.add_dish(forshmak_jewish)
    cafe.add_dish(forshmak_germany)
    cafe.add_dish(fish_cutlet_steam)

    assert ice_cream_strawberry.dish_type in cafe.cafe.keys()
    assert steak_beef.dish_type in cafe.cafe.keys()
    assert cutlet_pork.dish_type in cafe.cafe.keys()
    assert chop_beef.dish_type in cafe.cafe.keys()
    assert black_tea.dish_type in cafe.cafe.keys()
    assert espresso_coffee.dish_type in cafe.cafe.keys()
    assert light_beer.dish_type in cafe.cafe.keys()
    assert dark_beer.dish_type in cafe.cafe.keys()
    assert fried_fish_cod.dish_type in cafe.cafe.keys()
    assert fried_fish_pike.dish_type in cafe.cafe.keys()
    assert forshmak_jewish.dish_type in cafe.cafe.keys()
    assert forshmak_germany.dish_type in cafe.cafe.keys()
    assert fish_cutlet_steam.dish_type in cafe.cafe.keys()


def test_add_dish_by_category():
    cafe = cafe = Cafe('Cafe', 'Vasiliy Alibabaevich')

    dish_list = [IceCream('raspberry', 'very sweet', 60, 400, 900, 80, 300, 700),
                 IceCream('Strawberry', 'sweet', 60, 400, 900, 80, 300, 700)]

    assert len(cafe.cafe) == 0
    cafe.add_dish_by_category(category='Десерты', dishes=dish_list)
    assert len(cafe.cafe.get('Десерты', [])) == len(dish_list)


def test_str():
    pass


def test_get_dishes_by_category():
    cafe = cafe = Cafe('Cafe', 'Vasiliy Alibabaevich')

    dish_list = [IceCream('raspberry', 'very sweet', 60, 400, 900, 80, 300, 700),
                 IceCream('Strawberry', 'sweet', 60, 400, 900, 80, 300, 700)]

    cafe.add_dishes(dish_list)
    assert len(cafe.get_dishes_by_category('Десерты')) == 2
    assert len(cafe.get_dishes_by_category('Рыбные')) == 0


def test_get_dishes_types_with_max_count():
    pass


def test_get_dishes_types_with_min_count():
    pass
