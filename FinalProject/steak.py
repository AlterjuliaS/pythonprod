from FinalProject.meat import Meat


class Steak(Meat):
    """
    Стейк
    """

    def __init__(self, kind_of_meat, rare, price, calorie, proteins, fats, carbohydrates):
        super().__init__(kind_of_meat, price, calorie, proteins, fats, carbohydrates)
        self.rare = rare

    def __str__(self):
        return f"Steak (rare: {self.rare} | {super().__str__()}"
