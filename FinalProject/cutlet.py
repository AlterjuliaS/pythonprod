from FinalProject.meat import Meat


class Cutlet(Meat):
    """
    Котлета
    """

    def __init__(self, kind_of_meat, sauce, price, calorie, proteins, fats, carbohydrates):
        super().__init__(kind_of_meat, price, calorie, proteins, fats, carbohydrates)
        self.sauce = sauce

    def __str__(self):
        return f"Cutlet (Sauce: {self.sauce} | {super().__str__()}"
