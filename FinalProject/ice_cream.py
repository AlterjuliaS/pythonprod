from FinalProject.deserts import Deserts


class IceCream(Deserts):
    """
    Мороженое
    """
    def __init__(self, taste, sweetness, fat_content, price, calorie, proteins, fats, carbohydrates):
        super().__init__(taste, sweetness, price, calorie, proteins, fats, carbohydrates)
        self.fat_content = fat_content

    def __str__(self):
        return f"Ice Cream (taste: {self.taste} | sweetness: {self.sweetness} | " \
               f"fat content: {self.fat_content} | {super().__str__()}"

