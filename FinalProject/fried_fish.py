from FinalProject.fish import Fish


class FriedFish(Fish):
    """
    Жареная рыба
    """

    def __init__(self, cooking_method, kind_of_fish, description, price, calorie, proteins, fats, carbohydrates):
        super().__init__(cooking_method, kind_of_fish, price, calorie, proteins, fats, carbohydrates)
        self.description = description

    def __str__(self):
        return f"Fried Fish (Description: {self.description} | {super().__str__()}"
