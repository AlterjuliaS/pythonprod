from FinalProject.drinks import Drinks


class Tea(Drinks):
    """
    Чай
    """

    def __init__(self, kind_of_tea, alcohol, caffeine,
                 temperature, price, calorie, proteins, fats, carbohydrates):
        super().__init__(alcohol, caffeine, temperature, price, calorie, proteins, fats, carbohydrates)
        self.kind_of_tea = kind_of_tea

    def __str__(self):
        return f"Tea (Kind of tea: {self.kind_of_tea} | {super().__str__()}"
