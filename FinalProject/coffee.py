from FinalProject.drinks import Drinks


class Coffee(Drinks):
    """
    Кофе
    """

    def __init__(self, kind_of_coffee, with_milk, alcohol, caffeine,
                 temperature, price, calorie, proteins, fats, carbohydrates):
        super().__init__(alcohol, caffeine, temperature, price, calorie, proteins, fats, carbohydrates)
        self.kind_of_coffee = kind_of_coffee
        self.with_milk = with_milk

    def __str__(self):
        return f"Coffee (Kind of coffee: {self.kind_of_coffee} | With milk: {self.with_milk} | {super().__str__()}"
