from FinalProject.drinks import Drinks


class Beer(Drinks):
    """
    Пиво
    """
    def __init__(self, kind_of_beer, country, alcohol, caffeine,
                 temperature, price, calorie, proteins, fats, carbohydrates):
        super().__init__(alcohol, caffeine, temperature, price, calorie, proteins, fats, carbohydrates)
        self.kind_of_beer = kind_of_beer
        self.country = country

    def __str__(self):
        return f"Beer (Kind of beer: {self.kind_of_beer} | Country: {self.country} | {super().__str__()}"