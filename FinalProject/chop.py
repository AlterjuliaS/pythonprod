from FinalProject.meat import Meat


class Chop(Meat):
    """
    Отбивная
    """

    def __init__(self, kind_of_meat, cooking_method, price, calorie, proteins, fats, carbohydrates):
        super().__init__(kind_of_meat, price, calorie, proteins, fats, carbohydrates)
        self.cooking_method = cooking_method

    def __str__(self):
        return f"Chop (Cooking method: {self.cooking_method} | {super().__str__()}"