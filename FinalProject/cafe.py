import pickle

from FinalProject.beer import Beer
from FinalProject.chop import Chop
from FinalProject.coffee import Coffee
from FinalProject.cutlet import Cutlet
from FinalProject.fish_cutlet import FishCutlet
from FinalProject.forshmak import Forshmak
from FinalProject.fried_fish import FriedFish
from FinalProject.ice_cream import IceCream
from FinalProject.steak import Steak
from FinalProject.tea import Tea


class Cafe:

    def __init__(self, title, director, load_state=False):
        self.title = title
        self.director = director

        self.state_file_name = f'cafe_state_{self.title}_{self.director.f_name}_{self.director.patronymic}.pkl'

        if load_state:
            self.load_state()
        else:
            self.cafe = {}

    def save_state(self):
        with open(self.state_file_name, 'wb') as file:
            pickle.dump(self.cafe, file)

    def load_state(self):
        try:
            with open(self.state_file_name, 'rb') as file:
                self.cafe = pickle.load(file)
        except FileNotFoundError as ex:
            self.cafe = {}
            print(ex)

    def add_dish(self, dish):
        dish_type = dish.dish_type
        if self.cafe.get(dish_type, None) is None:
            self.cafe[dish_type] = []
        self.cafe[dish_type].append(dish)

        self.save_state()

    def add_dish_by_category(self, category: str, dishes: list):
        for dish in dishes:
            if dish.dish_type != category:
                raise Exception('Категория блюда и категория в меню не совпадает!')
            self.add_dish(dish)

        self.save_state()

    def add_dishes(self, dishes: list):
        for dish in dishes:
            self.add_dish(dish)

    def __str__(self):
        result_str = ""
        result_str += f"Cafe title: {self.title}\n"
        result_str += f"Cafe director: {self.director}\n"
        result_str += self.get_dishes_cafe_str()

        return result_str

    def get_dishes_cafe_str(self):
        result_str = ""
        for dish_type, dishes in self.cafe.items():
            result_str += f"{dish_type}: \n"
            for dish in dishes:
                result_str += f"\t{dish}\n"

        return result_str

    def get_dishes_by_category(self, category):
        return self.cafe.get(category, [])

    def get_dishes_by_category_str(self, category):
        dishes_by_category = self.cafe.get(category, [])
        result_str = f"Category: {category}\n"
        for dish in dishes_by_category:
            result_str += f"\t{dish}\n"

        return result_str

    def get_dishes_types_with_max_count(self):
        if len(self.cafe) == 0:
            return []

        dishes_count = {}
        for dishes_type, dishes in self.cafe.items():
            dishes_count[dishes_type] = len(dishes)

        max_count = max(dishes_count.values())

        result_list = []
        for dishes_type, counter in dishes_count.items():
            if counter == max_count:
                result_list.append(dishes_type)

        return result_list

    def get_dishes_types_with_min_count(self):
        if len(self.cafe) == 0:
            return []

        dishes_count = {}
        for dishes_type, dishes in self.cafe.items():
            dishes_count[dishes_type] = len(dishes)

        min_count = min(dishes_count.values())

        result_list = []
        for dishes_type, counter in dishes_count.items():
            if counter == min_count:
                result_list.append(dishes_type)

        return result_list


if __name__ == '__main__':
    cafe = Cafe('Cafe', 'Vasiliy Alibabaevich')
    ice_cream_strawberry = IceCream('strawberry', 'sweet', 50, 70, 700, 15, 40, 155)
    steak_beef = Steak('beef', 'medium', 100, 500, 150, 50, 70)
    cutlet_pork = Cutlet('pork', 'None', 100, 400, 70, 300, 700)
    chop_beef = Chop('beef', 'for a couple', 100, 400, 70, 300, 700)
    black_tea = Tea('black', 0, 0, 'hot', 50, 100, 5, 5, 150)
    espresso_coffee = Coffee('Arabika', 'No', 0, 'Yes', 'hot', 450, 300, 10, 50, 150)
    light_beer = Beer('Light', 'Czech Republic', 5, 0, 'cold', 250, 500, 0, 40, 600)
    dark_beer = Beer('Dark', 'Czech Republic', 6, 0, 'cold', 350, 500, 0, 40, 600)
    fried_fish_cod = FriedFish('Fried', 'Cod', 'Fillet', 1000, 900, 300, 200, 700)
    fried_fish_pike = FriedFish('Fried', 'Pike', 'Steak', 1000, 900, 300, 200, 700)
    forshmak_jewish = Forshmak('Mixed', 'Herring', 'Israel', 150, 800, 200, 400, 900)
    forshmak_germany = Forshmak('Baked', 'Herring and meat', 'Germany', 300, 1000, 500, 200, 300)
    fish_cutlet_steam = FishCutlet('Steam', 'Cod', 'Chopped', 700, 800, 300, 300, 200)

    cafe.add_dish(ice_cream_strawberry)
    cafe.add_dish(steak_beef)
    cafe.add_dish(cutlet_pork)
    cafe.add_dish(chop_beef)
    cafe.add_dish(black_tea)
    cafe.add_dish(espresso_coffee)
    cafe.add_dish(light_beer)
    cafe.add_dish(dark_beer)
    cafe.add_dish(fried_fish_cod)
    cafe.add_dish(fried_fish_pike)
    cafe.add_dish(forshmak_jewish)
    cafe.add_dish(forshmak_germany)
    cafe.add_dish(fish_cutlet_steam)

    print(cafe)

    print(cafe.get_dishes_by_category('Десерты'))
    print(cafe.get_dishes_by_category('1111'))

    print(cafe.get_dishes_types_with_max_count())

    print(cafe.get_dishes_types_with_min_count())

    dish_list = [IceCream('raspberry', 'very sweet', 60, 400, 900, 80, 300, 700),
                 IceCream('Strawberry', 'sweet', 60, 400, 900, 80, 300, 700)]

    try:
        cafe.add_dish_by_category(category='Десерты', dishes=dish_list)
    except Exception as ex:
        print('ERROR: ', ex)
    else:
        print("Добавление прошло успешно")
