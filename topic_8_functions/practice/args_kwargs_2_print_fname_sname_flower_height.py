def print_fname_sname_flower_height(name, family, **flowers):
    """
    Функция print_fname_sname_flower_height.

    Принимает 3 аргумента:
    имя,
    фамилию владельца цветов,
    именованные аргументы с высотой имеющихся цветов (**kwargs).

    Выводит на экран имя и фамилию, а затем цветок и высоты этого цветка (может быть несколько).

    Пример вызова функции:
    print_fname_sname_flower_height('Лидия', 'Петрова', rose=[50, 77, 80], tulp=60, magic_flower=[444, 555, 777]).
    """
    print(f'{name} {family}: ')
    for flower_name, flower_h in flowers.items():
        print(f'\t{flower_name} => {flower_h}')


if __name__ == '__main__':
    print_fname_sname_flower_height('Лидия', 'Петрова', rose=[50, 77, 80], tulp=60, magic_flower=[444, 555, 777])
