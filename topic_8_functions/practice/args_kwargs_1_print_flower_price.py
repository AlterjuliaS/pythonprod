def print_flower_price(name, *prices):
    """
    Функция print_flower_price.

    Принимает 2 аргумента:
    название цветка (строка),
    неопределенное количество цен (*args).

    Функция print_flower_price выводит вначале название цветка,
    а потом все цены на него.

    В функции main вызывается функция print_flower_price и передается название цветка
    и произвольное количество цен.

    Пример: print_flower_price('rose', 77, 10, 50, 99)
    или print_flower_price('tulp', 100, 44, 777, 876, 555, 111).
    """
    print(name, end=':')
    for price in prices:
        print(price, end=' ')
    print()


if __name__ == '__main__':
    print_flower_price('rose', 77, 10, 50, 99)
    print_flower_price('tulp', 160, 44, [888, 777, 555], {876: [664]}, 555, 111)
