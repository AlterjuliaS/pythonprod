import json

def save_set_to_file_json(path, my_dict):
    """
    Функция save_set_to_file_json.

    Принимает 2 аргумента:
        строка (название файла или полный путь к файлу),
        словарь JSON (для сохранения).

    Сохраняет словарь (JSON) в файл.
    Загрузить словарь JSON (прочитать файл),
    проверить его на корректность.
    """
    with open(path, 'w') as file:
        json.dump(my_dict, file, indent=2, sort_keys=False)


if __name__ == '__main__':
    my_dict = {
        'Маша': [1, 2, 3],
        'ОЛОЛО': {1: 'one',
                  2: 'two'},
        'set': (9.844, 3.222)
    }

    save_set_to_file_json('practice_local.json', my_dict)