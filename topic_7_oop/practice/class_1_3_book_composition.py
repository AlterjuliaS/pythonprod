from topic_7_oop.practice.class_1_1_page import Page
from topic_7_oop.practice.class_1_2_cover import Cover


class BookWithComposition:
    """
    Класс Книга
    Поля:
        страницы,
        название (каждое слово с большой буквы, остальные маленькие),
        автор,
        год издания
    Методы:
        вывести весь контент,
        добавить страницу в конец,
        добавить несколько страниц в конец,
        получить количество страниц (обложки, а их может быть любое количество),
        вернуть все четные страницы
        __invert__: вернуть результат реверса строки с названием
                    (например, было "Крутая Книга", а стало "Агинк Яатурк").
        __add__: добавить к году число
    """

    def __init__(self, title: str, author: str, year: int):
        self.pages = []
        self.title = title.title()
        self.author = author.capitalize()
        self.year = year

    def print_content(self):
        print(f'Название: {self.title}')
        print(f'Автор: {self.author}')
        print(f'Год: {self.year}')
        for p in self.pages:
            p.print_content()

    def append_cover(self, n, c, color, header):
        self.pages.append(Cover(n, c, color, header))  # представим, что тут может быть что-то более сложное

    def append_page(self, num, content):
        self.pages.append(Page(num, content))     # представим, что тут может быть что-то более сложное

    def extend_pages(self, my_params):
        for param in my_params:
            if len(param) == 2:
                self.pages.append(Page(param[0], param[1]))
            elif len(param) == 4:
                self.pages.append(Cover(param[0], param[1], param[2], param[3]))

    def page_count(self):
        counter = 0
        for p in self.pages:
            if type(p) == Page:
                counter += 1    # counter = counter + 1

        return counter

    def get_even_pages(self):
        return [p for p in self.pages if p.num % 2 == 0]    # list comprehension

    def __invert__(self):   # перегрузка ~
        self.title = self.title[::-1].title()
        return self

    def __add__(self, other):   # перегрузка +
        if type(other) == int:
            self.year += other

        return self


if __name__ == '__main__':
    string = 'TeSt hTsE'
    print(string.lower())
    print(string.upper())
    print(string.capitalize())
    print(string.title())

    print('-----------------------------------------------------------------------------------------------------------')

    book1 = BookWithComposition('КниГа для людеЙ', "ваcЯ", 2020)
    book1.print_content()

    print('-----------------------------------------------------------------------------------------------------------')

    book1.append_cover(0, 'лялял', "Зеленого", "Выбор чителей!")
    book1.append_page(1, 'Жили были')

    params = [(2, 'wert'), (3, 'yuii'), (4, 'rtrtr', "Красного", "Книга года!")]
    book1.extend_pages(params)  # book1.pages.extend(pages2)

    book1.print_content()

    print(book1.page_count())

    print([str(p) for p in book1.get_even_pages()])

    print((~book1).title)

    book1 += 3
    print(book1.year)
    print((book1 + 6).year)

