from topic_7_oop.practice.class_1_1_page import Page


class Cover(Page):
    def __init__(self, n, c, color, header):
        super().__init__(n, c)
        self.color = color
        self.header = header

    def print_content(self):
        print(self)
        super(Cover, self).print_content()

    def __str__(self):
        return f'Страница {self.color} цвета\nЗаголовок {self.header}'


if __name__ == '__main__':
    c = Cover(0, 'bobobobo', 'Синий', 'Лучшая книга века!')
    c.print_content()
