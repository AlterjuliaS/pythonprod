from topic_7_oop.practice.class_1_1_page import Page
from topic_7_oop.practice.class_1_2_cover import Cover


class BookWithAggregation:
    """
    Класс Книга
    Поля:
        страницы,
        название (каждое слово с большой буквы, остальные маленькие),
        автор,
        год издания
    Методы:
        вывести весь контент,
        добавить страницу в конец,
        добавить несколько страниц в конец,
        получить количество страниц (обложки, а их может быть любое количество),
        вернуть все четные страницы
        __invert__: вернуть результат реверса строки с названием
                    (например, было "Крутая Книга", а стало "Агинк Яатурк").
        __add__: добавить к году число
    """

    def __init__(self, title: str, author: str, year: int):
        self.pages = []
        self.title = title.title()
        self.author = author.capitalize()
        self.year = year

    def print_content(self):
        print(f'Название: {self.title}')
        print(f'Автор: {self.author}')
        print(f'Год: {self.year}')
        for p in self.pages:
            p.print_content()

    def append_page(self, page):
        self.pages.append(page)

    def extend_pages(self, pages):
        self.pages.extend(pages)

    def page_count(self):
        count = 0
        for p in self.pages:
            if type(p) == Page:
                count += 1
        return count

    def get_even_pages(self):
        return [p for p in self.pages if p.num % 2 == 0]

    def __invert__(self):
        self.title = self.title[::-1].title()
        return self

    def __add__(self, other):
        if type(other) == int:
            self.year += other
        return self


if __name__ == '__main__':
    book1 = BookWithAggregation('КниГа', 'вася', 2020)
    book1.print_content()

    print('----------------------------------------------------------------------------------------------')

    cover1 = Cover(0, 'лялялял', 'Зеленый', 'Выбор читателей!')
    page1 = Page(1, 'Жили были!')

    book1.append_page(cover1)
    book1.append_page(page1)

    pages2 = [Page(2, 'wert'), Page(3, 'vuii')]
    book1.extend_pages(pages2)

    book1.print_content()

    print(book1.page_count())

    print([str(p) for p in book1.get_even_pages()])

    print((~book1).title)

    book1 += 3
    print(book1.year)
