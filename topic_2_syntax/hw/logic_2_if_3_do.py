"""
Функция if_3_do.

Принимает число.
Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
Вернуть результат.
"""


def if_3_do(n):
    if n > 3:
        return n + 10
    else:
        return n - 10


def main():
    print(if_3_do(15))
    print(if_3_do(2))


if __name__ == '__main__':
    main()
