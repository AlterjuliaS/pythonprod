"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


def count_odd_num(n):
    if not isinstance(n, int) or isinstance(n, bool):
        return "Must be int!"
    elif n <= 0:
        return "Must be > 0!"
    else:
        odd = 0
        while n > 0:
            if n % 2 == 0:
                n = n // 10
            else:
                odd = odd + 1
                n = n // 10
        return odd


def main():
    print(count_odd_num(222))


if __name__ == '__main__':
    main()
