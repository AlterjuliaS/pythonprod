"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(str1, str2):
    if len(str1) > len(str2):
        if str1.find(str2) != -1:
            return True
        else:
            return False
    elif len(str2) > len(str1):
        if str2.find(str1) != -1:
            return True
        else:
            return False
    elif len(str1) == len(str2):
        return False
    elif len(str1) == 0 or len(str2) == 0:
        return True


def main():
    print(check_substr('dsafd34234', '342'))
    print(check_substr('fdsfdsfds', '1111'))
    print(check_substr('bbb', 'bbb'))
    print(check_substr('bbb', ''))


if __name__ == '__main__':
    main()
