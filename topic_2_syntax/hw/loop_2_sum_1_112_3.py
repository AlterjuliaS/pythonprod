"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    acc = 1
    summa = 0
    while acc < 114:
        summa = summa + acc
        acc += 3
    return summa


def main():
    print(sum_1_112_3())


if __name__ == '__main__':
    main()
