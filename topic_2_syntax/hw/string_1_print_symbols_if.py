"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(s):
    if len(s) == 0:
        return print("Empty string!")
    elif len(s) > 5:
        return print(s[0:3]+s[-3:])
    else:
        return print(s[0]*len(s))


def main():
    print_symbols_if('')
    print_symbols_if('123fsfds534534')
    print_symbols_if('00000')


if __name__ == '__main__':
    main()
