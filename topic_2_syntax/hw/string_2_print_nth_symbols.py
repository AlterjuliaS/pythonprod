"""
Функция print_nth_symbols.

Принимает строку и натуральное число n (целое число > 0).
Вывести символы с индексом n, n*2, n*3 и так далее.
Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'

Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
Если тип n не int, то вывести строку 'Must be int!'.

Если n больше длины строки, то вывести пустую строку.
"""


def print_nth_symbols(s, n):
    if not isinstance(n, int) or isinstance(n, bool):
        return print("Must be int!")
    elif n <= 0:
        return print("Must be > 0!")
    elif n > len(s):
        return print(" ")
    else:
        str2 = ''
        while n < len(s):
            str1 = s[n]
            str2 = str2 + str1
            n = n + n
        return print(str2)


def main():
    print_nth_symbols('123fdsfdgdfy566756', -1)
    print_nth_symbols('dasdasdasd', 4_23)
    print_nth_symbols('2131321321321', 5)


if __name__ == '__main__':
    main()
