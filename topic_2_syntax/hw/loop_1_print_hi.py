"""
Функция print_hi.

Принимает число n.
Выведите на экран n раз фразу "Hi, friend!"
Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
"""


def print_hi(n):
    str1 = "Hi, friend!" * n
    return print(str1)


def main():
    print_hi(0)


if __name__ == '__main__':
    main()
